import { Pact, Interaction, InteractionObject } from '@pact-foundation/pact';
import { resolve } from 'path';

export class PactWrapper {
    private readonly provider: Pact;

    constructor(providerTkNameId: string) {
        this.provider = new Pact({
            // the port must be the same as in jest config pact
            port: 8181,
            log: resolve(process.cwd(), 'pacts', 'logs', 'pact.log'),
            dir: resolve(process.cwd(), 'pacts'),
            spec: 3,
            logLevel: 'info',
            consumer: 'ngrx-quickstarter-rwc',
            provider: providerTkNameId,
        });
    }

    async init(): Promise<void> {
        try {
            await this.provider.setup();
        } catch (error) {
            console.error(error);
        }
    }

    async verify(): Promise<void> {
        try {
            await this.provider.verify();
        } catch (error) {
            console.error(error);
        }
    }

    async finalize(): Promise<void> {
        try {
            await this.provider.finalize();
        } catch (error) {
            console.error(error);
        }
    }

    async addInteraction(interaction: Interaction | InteractionObject): Promise<void> {
        try {
            await this.provider.addInteraction(interaction);
        } catch (error) {
            console.error(error);
        }
    }
}
