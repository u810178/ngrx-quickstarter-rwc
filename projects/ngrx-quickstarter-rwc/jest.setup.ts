// WARNING: jest-preset-angular must be the first import!
import 'jest-preset-angular/setup-jest';

// Globals mocks
window.matchMedia =
    window.matchMedia ||
    (() =>
        ({
            matches: false,
            addListener: () => {},
            removeListener: () => {},
        } as unknown as MediaQueryList));

//mock sync rwc config
Object.defineProperty(document.defaultView, 'MOBI_RWC_CONFIG', { value: {} });
