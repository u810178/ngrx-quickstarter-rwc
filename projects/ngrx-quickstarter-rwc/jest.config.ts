import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
    preset: 'jest-preset-angular',
    setupFilesAfterEnv: ['<rootDir>/jest.setup.ts'],
    coverageDirectory: '../../coverage',
    coverageReporters: ['html', 'text-summary', 'cobertura'],
    collectCoverageFrom: [
        '**/src/{lib,app}/**/*.{ts,js}',
        '!**/public{_,-}api.ts',
        '!**/index.ts',
        '!**/*.module.ts',
        '!**/*.spec.pact.ts',
    ],
    moduleNameMapper: {
        '^raw-loader!(.*)$': '<rootDir>/jest.mocks.ts',
    },
    reporters: ['default', ['jest-junit', { outputDirectory: 'test-results' }]],
    // TODO process ESM modules like @angular/* (import syntax) remove once jest is switched to ESM
    transformIgnorePatterns: ['node_modules/(?!(@angular|date-fns|lodash-es))'],
    maxWorkers: 4,
    globalSetup: 'jest-preset-angular/global-setup',
};
export default config;
