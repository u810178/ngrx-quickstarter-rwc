import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { DummyService } from './dummy.service';

describe('Dummy Service', () => {
    let dummyService: DummyService;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [DummyService],
        });
        dummyService = TestBed.inject(DummyService);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    it('should exists', () => {
        expect(dummyService).toBeDefined();
    });

    it('spec name', () => {
        dummyService.get(17).subscribe();
        httpTestingController.expectOne({ method: 'GET', url: 'api/dummy/17' }).flush('');
    });

    afterEach(() => {
        // After every test, assert that there are no more pending requests.
        httpTestingController.verify();
    });
});
