import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Matchers } from '@pact-foundation/pact';

import { PactWrapper } from '../../../../pact-wrapper.pact';

import { DummyService, User } from './dummy.service';

describe('Dummy Service Pact', () => {
    const provider = new PactWrapper('backend-tk-name-id');

    beforeAll(async () => {
        await provider.init();
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [DummyService],
        });
    });

    afterEach(async () => {
        await provider.verify();
    });

    afterAll(async () => {
        await provider.finalize();
    });

    describe('get()', () => {
        const userId = 1;

        const expectedUser: User = {
            id: 1,
            firstName: 'Toto',
            lastName: 'Titi',
        };

        beforeAll(async () => {
            await provider.addInteraction({
                state: `user 1 exists`,
                uponReceiving: 'a request to GET a user',
                withRequest: {
                    method: 'GET',
                    path: `/api/dummy/${userId}`,
                },
                willRespondWith: {
                    status: 200,
                    body: {
                        id: expectedUser.id,
                        firstName: Matchers.string(expectedUser.firstName),
                        lastName: Matchers.string(expectedUser.lastName),
                    },
                },
            });
        });

        it('should get user with id 1', async () => {
            const userService: DummyService = TestBed.inject(DummyService);
            const response = await userService.get(userId).toPromise();
            expect(response).toEqual(expectedUser);
        });
    });
});
