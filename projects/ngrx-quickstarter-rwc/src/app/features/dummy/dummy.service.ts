import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export interface User {
    id: number;
    firstName: string;
    lastName: string;
}

/**
 * This is only a dummy service that provides a pact test example.
 * You should delete this file
 */
@Injectable({
    providedIn: 'root',
})
export class DummyService {
    private BASE_URL = 'api/dummy';

    constructor(private httpClient: HttpClient) {}

    get(id: number): Observable<User> {
        return this.httpClient.get<User>(`${this.BASE_URL}/${id}`);
    }
}
