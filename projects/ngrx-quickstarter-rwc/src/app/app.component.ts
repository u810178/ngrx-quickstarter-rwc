import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { takeUntil } from 'rxjs/operators';

import { Subject } from 'rxjs';

import { selectBookCollection, selectBooks } from './state/books.selectors';
import { retrievedBookList, addBook, removeBook } from './state/books.actions';
import { GoogleBooksService } from './book-list/books.service';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit, OnDestroy {
    private destroy$: Subject<void> = new Subject<void>();

    books$ = this.store.select(selectBooks);
    bookCollection$ = this.store.select(selectBookCollection);

    constructor(private booksService: GoogleBooksService, private store: Store) {}

    ngOnInit() {
        this.booksService
            .getBooks()
            .pipe(takeUntil(this.destroy$))
            .subscribe(books => this.store.dispatch(retrievedBookList({ books })));
    }

    onAdd(bookId: string) {
        this.store.dispatch(addBook({ bookId }));
    }

    onRemove(bookId: string) {
        this.store.dispatch(removeBook({ bookId }));
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }
}
