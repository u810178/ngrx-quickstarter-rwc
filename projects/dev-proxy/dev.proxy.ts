import { MockBackend, MODE } from '@mobi/rwc-utils-devtools-jslib';

module.exports = MockBackend.create(process.env.MOCK_BACKEND_MODE as MODE, __dirname + '/recordings')
    .init()
    .mockLoginOnReplay()
    .startLoginOnRecord()
    .createProxyConfiguration({ tkNameId: 'ngrx-quickstarter-rwc' });
