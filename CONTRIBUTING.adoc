= How to contribute
:toc:
:toclevels: 3

== Conventional Commits

The commit message should respect the https://www.conventionalcommits.org[conventional commits] and contains a JIRA ticket.
Each commit message consists of a *header*, and optionally a *body* and a *footer*.
These conventions are enforced by a commit-msg git hook.
The rule settings are those of the https://github.com/conventional-changelog/commitlint/blob/bc5a3287104b06ab3b6ec7ee0edb1e6f02448e81/%40commitlint/config-conventional/index.js[@commitlint/config-conventional] package.

TIP: The JIRA ticket can be *anywhere* in the commit message.

=== Format of the Commit Message

[source]
----
<type>(optional scope): <short summary>

[optional body]

[optional footer(s)]
----

==== Message Subject (first line)

[source]
----
<type>(<optional scope>): <short summary>
  │       │                  │
  │       │                  └─> Summary in present tense. Not capitalized. No period at the end.
  │       │
  │       └─> Commit Scope: the scope without spaces and capitals.
  │
  └─> Commit Type: feat|fix|perf|chore|refactor|docs|style|test|revert|ci|build (see below)
----

.Allowed `<type>` values
[%header,cols="1s,^1,7"]
|===
| type | release | description
| feat | minor | A new feature
| fix | patch | A bug fix
| perf | patch | A code change that improves performance
| chore | patch | Changes to the build process or auxiliary tools and libraries
| refactor | patch | A code change that neither fixes a bug nor adds a feature
| revert | patch | To revert a previous commit
| docs | - | Documentation only changes
| style | - | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
| test | - | Adding missing or correcting existing tests
| ci | - | Changes to the CI configuration files and scripts (example scopes: teamcity, gitlab)
| build | - | Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
|===

Depends on the https://github.com/conventional-changelog/commitlint/tree/master/%40commitlint/config-conventional[<type>],
the release could increase the major, minor, patch or do not create a new release.

See the following links for more details:

- https://github.com/semantic-release/commit-analyzer/blob/master/lib/default-release-rules.js[Semantic release default release rules]
- https://gitlab.com/diemobiliar/it/rwc/deliver/rwc-deliver-cli-jslib/-/blob/master/assets/release/.releaserc.js#L3[RWC extends release rules]

CAUTION: If no new release are created, the pipeline will crash in the next stage.

The commit messages will be then used in the gitlab pipeline by
https://github.com/semantic-release/semantic-release[semantic-release] to provide the release notes.

==== Message Body

- uses the imperative, present tense: “change” not “changed” nor “changes”
- includes motivation for the change and contrasts with previous behavior

== Get the Changelog or Release Notes

In Gitlab, on the left menu navigate to _Project Overview_ -> _Releases_.
